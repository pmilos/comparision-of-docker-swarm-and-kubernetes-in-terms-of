The main goal of the thesis was to present the concept orchestrator, what it is
and how Docker works. Then a comparison of Docker Swarm and Kubernetes
orchestrators in terms of architecture. It shows how to install and confgure
a test environment containing selected tools. The platforms have been
compared for selected functions such as how to run basic applications, load
balancing, user session maintenance, the application's update scenario and the
MariaDB database installation. Performance tests were also carried out by
recording data and sending inserts to databases.

PDF in section "Downloads"